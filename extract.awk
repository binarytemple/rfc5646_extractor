#!/usr/bin/awk -f 
function ltrim(s) { sub(/^[ \t]+/, "", s); return s }
function rtrim(s) { sub(/[ \t]+$/, "", s); return s }
function trim(s)  { return rtrim(ltrim(s)); }

BEGIN { FS = "\n"; RS = "%%\n" ; OFS=","; ORS="\n" }

/^Type: language/ { 
  n=split($2,type,":")
  p=split($3,desc,":")
  print trim(type[2])
  print trim(desc[2])
}
